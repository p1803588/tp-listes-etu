#include "liste.hpp"

#include <iostream>
#include <cassert>
using namespace std;

Liste::Liste()
{
  debut = nullptr;
}

Liste::Liste(const Liste &autre)
{
  if (autre.debut == nullptr)
  {
    debut = nullptr;
  }
  else
  {
    Cellule *c = new Cellule;
    debut = c;
    c->valeur = autre.debut->valeur;
    for (Cellule *o = autre.debut->prochain; o; o = o->prochain)
    {
      c->prochain = new Cellule;
      c = c->prochain;
      c->valeur = o->valeur;
    }
  }
}

Liste &Liste::operator=(const Liste &autre)
{

  if (debut)
  {
    for (Cellule *c = debut; c;)
    {
      Cellule *n = c->prochain;
      delete c;
      c = n;
    }
  }
  if (autre.debut == nullptr)
  {
    debut = nullptr;
  }
  else
  {
    Cellule *c = new Cellule;
    debut = c;
    c->valeur = autre.debut->valeur;
    for (Cellule *o = autre.debut->prochain; o; o = o->prochain)
    {
      c->prochain = new Cellule;
      c = c->prochain;
      c->valeur = o->valeur;
    }
  }
  return *this;
}

Liste::~Liste()
{
  if (debut)
  {
    for (Cellule *c = debut; c;)
    {
      Cellule *n = c->prochain;
      delete c;
      c = n;
    }
  }
}

void Liste::ajouter_en_tete(int val)
{
  Cellule *c = new Cellule;
  c->prochain = debut;
  c->valeur = val;
  debut = c;
}

void Liste::ajouter_en_queue(int val)
{

  Cellule *c = new Cellule;
  c->valeur = val;
  c->prochain = nullptr;
  queue()->prochain = c;
}

void Liste::supprimer_en_tete()
{

  assert(debut);
  Cellule *c = debut;
  debut = c->prochain;
  delete c;
}

Cellule *Liste::tete()
{
  return debut;
}

const Cellule *Liste::tete() const
{
  return debut;
}

Cellule *Liste::queue()
{
  Cellule *c = debut;
  for (; c && c->prochain; c = c->prochain)
    ;
  return c;
}

const Cellule *Liste::queue() const
{
  Cellule *c = debut;
  for (; c && c->prochain; c = c->prochain)
    ;
  return c;
}

int Liste::taille() const
{
  int i = 0;
  for (Cellule *c = debut; c; c = c->prochain)
    i++;
  return i;
}

Cellule *Liste::recherche(int val)
{
  for (Cellule *c = debut; c; c = c->prochain)
  {
    if (c->valeur == val)
      return c;
  }
  return nullptr;
}

const Cellule *Liste::recherche(int val) const
{
  for (Cellule *c = debut; c; c = c->prochain)
  {
    if (c->valeur == val)
      return c;
  }
  return nullptr;
}

void Liste::afficher() const
{
  if (!debut)
  {
    cout << "[" << endl;
    return;
  }
  cout << "[ ";
  for (Cellule *c = debut; c; c = c->prochain)
    cout << c->valeur << " ";
  cout << "]" << endl;
}
